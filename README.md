# README #

Windows Holographic Application for 3d database roster visualization. The application maps database entities to 3D game objects in Unity.

### Setup ###

* Install neo4j community edition and start the server
* Run the queries in Neo4jUnity/neo4jUnityNew/Clusters-db_queries.txt file to set up the database
* In the GameController script, change the url of the post request to database to point to public ip address of 
  the machine where neo4j server is running. (Obtained using ipconfig command in Windows machine)
* Deploy application in hololens

### Guidelines ###

* Gaze over each game object to display its name on top of it.
* Tap on the ground and move your gaze to move the setup to a different location and tap again to place it in the new 
  location.