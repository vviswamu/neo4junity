﻿using UnityEngine;
using System.Text.RegularExpressions;
public class WorldCursor : MonoBehaviour
{
    private MeshRenderer meshRenderer;
    public static bool first = true;
    public static string prev = null;
    public static string current = null;
    public static GameObject prevOb = null;


    // Use this for initialization
    void Start()
    {
        // Grab the mesh renderer that's on the same object as this script.
        meshRenderer = this.gameObject.GetComponentInChildren<MeshRenderer>();
        
    }

    // Update is called once per frame
    void Update()
    {
        // Do a raycast into the world based on the user's
        // head position and orientation.
        var headPosition = Camera.main.transform.position;
        var gazeDirection = Camera.main.transform.forward;

        RaycastHit hitInfo;

        if (Physics.Raycast(headPosition, gazeDirection, out hitInfo))
        {
            // If the raycast hit a hologram...
            // Display the cursor mesh.
            meshRenderer.enabled = true;

            // Move the cursor to the point where the raycast hit.
            this.transform.position = hitInfo.point;

            // Rotate the cursor to hug the surface of the hologram.
            this.transform.rotation = Quaternion.FromToRotation(Vector3.up, hitInfo.normal);
            //Debug.Log(hitInfo.collider.gameObject.name);
            current = hitInfo.collider.gameObject.name;
            Match match = Regex.Match(current, "^spatial");
            Match match2 = Regex.Match(current, "Mobile|Image|Computational");
            //if (current != prev && !(match.Success)  && !(match2.Success)) {
            if (current != prev && !(match.Success))
            {
                Destroy(prevOb);
                GameObject name = new GameObject();
                name.transform.position = hitInfo.point + new Vector3(-0.2f,0.2f,0);
                name.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
                name.AddComponent(typeof(TextMesh));
                TextMesh tempText = name.GetComponent<TextMesh>();
                tempText.text = hitInfo.collider.gameObject.name;
                prev = current;
                prevOb = name;

            }

          

        }
        else
        {
            // If the raycast did not hit a hologram, hide the cursor mesh.
            meshRenderer.enabled = false;
        }
    }
}