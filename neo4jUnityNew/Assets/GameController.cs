﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Collections.Specialized;
using SimpleJSON;
using System.Net;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour
{
    //public GameObject name;
    public GameObject persons;
    public GameObject[] personArray;
    GameObject[] personNameArray;
    public GameObject clusters;
    public GameObject[] clusterArray;
    GameObject[] clusterNameArray;
    private Vector3 screenPoint;
    private Vector3 offset;
    List<string> person_list;
    List<string> cluster_list;



    //class for Person Objects
    public class Person
    {
        public String name;
        public string type;
        public string prefab;
    }

    //class for Cluster Objects
    public class Cluster
    {
        public String name;
        public string prefab;
        public List<Person> mPerson = new List<Person>();
    }

    public class PersonCluster
    {
        public List<List<String>> person_cluster;
    }

    public class jsonValue
    {
        public List<string> columns;
        public List<List<string>> data;
    }

    Dictionary<string, Person> personMap = new Dictionary<string, Person>();
    Dictionary<string, Cluster> clusterMap = new Dictionary<string, Cluster>();

    public class CoroutineWithData
    {
        public Coroutine coroutine { get; private set; }
        public object result;
        private IEnumerator target;
        public CoroutineWithData(MonoBehaviour owner, IEnumerator target)
        {
            this.target = target;
            this.coroutine = owner.StartCoroutine(Run());
        }

        private IEnumerator Run()
        {
            while (target.MoveNext())
            {
                result = target.Current;
                yield return result;
            }
        }
    }



    class Program
    {
        Person ap1 = new Person();
        Dictionary<string, Person> personDict = new Dictionary<string, Person>();

        //function returning a hashmap of <PersonName, PersonObject>
        public Dictionary<string, Person> getPerson(JSONNode n)
        {
            //var personDict = new Dictionary<string, Person>();
            var result = n["data"].AsArray;

            foreach (JSONNode rec in result)
            {
                //var record = rec.AsArray;
                var record = rec;
                if (!personDict.ContainsKey(record[0].ToString()))
                {
                    Person ap = new Person();
                    ap.name = record[0].ToString();
                    ap.type = record[1].ToString();
                    ap.prefab = record[2].ToString();
                    personDict.Add(ap.name, ap);
                }

            }
            return personDict;
        }

        //function returning a hashmap of <ClusterName, ClusterObject>
        public Dictionary<string, Cluster> getCluster(JSONNode n)
        {
            var clusterDict = new Dictionary<string, Cluster>();
            var personDict = new Dictionary<string, Person>();
            var result = n["data"].AsArray;

            foreach (JSONNode rec in result)
            {
                var record = rec.AsArray;
                Cluster mc;
                if (!clusterDict.ContainsKey(record[0].ToString()))
                {
                    mc = new Cluster();
                    mc.name = record[0].ToString();
                    mc.prefab = record[4].ToString();
                    clusterDict.Add(mc.name, mc);
                }
                else
                {
                    mc = clusterDict[record[0].ToString()];
                }

                Person ap;
                if (!personDict.ContainsKey(record[1].ToString()))
                {
                    ap = new Person();
                    ap.name = record[1].ToString();
                    ap.type = record[2].ToString();
                    ap.prefab = record[3].ToString();
                    personDict.Add(ap.name, ap);
                }
                else
                {
                    ap = personDict[record[1].ToString()];
                }
                mc.mPerson.Add(ap);

            }

            return clusterDict;

        }
    }

    public Renderer rend;
    IEnumerator Start()
    {
       rend = GetComponent<Renderer>();

        CoroutineWithData cd = new CoroutineWithData(this, Upload("persons"));
        yield return cd.coroutine;
        JSONNode hp_persons_obj = (JSONNode)cd.result;

        CoroutineWithData cd1 = new CoroutineWithData(this, Upload("clusters"));
        yield return cd1.coroutine;
        JSONNode hp_clusters_obj = (JSONNode)cd1.result;


        CoroutineWithData cd2 = new CoroutineWithData(this, Upload("person-cluster"));
        yield return cd2.coroutine;
        JSONNode hp_persons_clusters_obj = (JSONNode)cd2.result;


        Program p = new Program();
        personMap = new Dictionary<string, Person>();
        clusterMap = new Dictionary<string, Cluster>();
        personMap = p.getPerson(hp_persons_obj);
        clusterMap = p.getCluster(hp_persons_clusters_obj);

      

        createSpheres(personMap, clusterMap);

    }




    IEnumerator Upload(string cdata)
    {
        string postData = "";
        if (cdata == "persons")
        {
            postData = "{ \"query\" : \"MATCH (n:Person) return n.name, n.type, n.prefab\"}";
        }
        else if (cdata == "clusters")
        {
            postData = "{ \"query\" : \"MATCH (m:Cluster) return m.name, m.prefab \"}";
        }
        else if (cdata == "person-cluster")
        {
            postData = "{ \"query\" : \"MATCH (n:Person)-[:BELONGS_TO]->(m:Cluster) return m.name, n.name, n.type, n.prefab, m.prefab \"}";
        }

        var request = new UnityWebRequest("http://192.168.0.103:7474/db/data/cypher", "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(postData);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");

        yield return request.Send();


        JSONNode j = JSON.Parse(request.downloadHandler.text);
        print("response");
        print(j);
        yield return j;

    }

    public Vector3 RandomCircle(Vector3 center, float radius, float a)
    {
        float ang = a;
        Vector3 pos;
        pos.x = center.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.z = center.z + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
        pos.y = center.y - 1 ;
        return pos;
    }

    void createSpheres(Dictionary<string, Person> personMap, Dictionary<string, Cluster> clusterMap)
    {
        GameObject ground = GameObject.Find("Ground");
        person_list = personMap.Keys.ToList<string>();
        cluster_list = clusterMap.Keys.ToList<string>();

        personArray = new GameObject[person_list.Count];
        personNameArray = new GameObject[person_list.Count];
        clusterArray = new GameObject[cluster_list.Count];
        clusterNameArray = new GameObject[cluster_list.Count];


        //Vector3 initialClusterPosition = new Vector3(-5, 1.5f, 0.2f);
        //Vector3 tempClusterNamePosition = initialClusterPosition + new Vector3(0, 0.1f, 0);

        //distance between clusters, 0 for first cluster
        float k = 0;

        //initial position for each cluster and people
        float x = -0.7f;
        float y = -0.7f;
        float z = 1.7f;

        for (int i = 0; i < cluster_list.Count; i++)
        {
            string cluster_path = clusterMap[cluster_list[i]].prefab;

            var cluster_str_old = clusterMap[cluster_list[i]].prefab.ToString();
            var cluster_str = cluster_str_old.Replace(@"""", "");
            clusterArray[i] = (GameObject)Instantiate(Resources.Load(cluster_str));
            

            GameObject name = new GameObject();
            //clusterNameArray[i] = (GameObject)Instantiate(name, tempClusterNamePosition, Quaternion.identity);
            //clusterNameArray[i].AddComponent(typeof(TextMesh));
            //TextMesh tempText = clusterNameArray[i].GetComponent<TextMesh>();
            //tempText.text = cluster_list[i];



            Vector3 newClusterPosition = new Vector3(x + k, y, z);
            k = k + 0.7f;
            
            clusterArray[i].transform.position = newClusterPosition;
            //clusterNameArray[i].transform.position = newClusterPosition + new Vector3(-1.5f, 2, 0);

            clusterArray[i].transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
            //clusterNameArray[i].transform.localScale -= new Vector3(0.39f, 0.39f, 0.39f);
            clusterArray[i].name = cluster_list[i];
            clusterArray[i].transform.SetParent(ground.transform);
            //clusterNameArray[i].transform.SetParent(ground.transform);

            var boxCollider = (BoxCollider)clusterArray[i].AddComponent<BoxCollider>();
            boxCollider.size = new Vector3(0.1f, 0.2f, 0.1f);

            int person_count = clusterMap[cluster_list[i]].mPerson.Count;
            personArray = new GameObject[person_count];
            newClusterPosition = newClusterPosition + new Vector3(0, 0.8f, 0);

            for (int j = 0; j < person_count; j++)
            {

                string path = clusterMap[cluster_list[i]].mPerson[j].prefab;
                var str_old = clusterMap[cluster_list[i]].mPerson[j].prefab.ToString();
                var str = str_old.Replace(@"""", "");
                personArray[j] = (GameObject)Instantiate(Resources.Load(str));



                var ran_angle = 360 / person_count;
                var a = j * ran_angle;

                Vector3 newPersonPosition = RandomCircle(newClusterPosition, 0.2f, a);
                personArray[j].transform.position = newPersonPosition;
                personArray[j].transform.SetParent(ground.transform);


                personArray[j].transform.localRotation = new Quaternion(0, 180, 0, 0);
                personArray[j].transform.localScale += new Vector3(0.01f, 0.01f, 0.01f);
                


               
                personArray[j].name = clusterMap[cluster_list[i]].mPerson[j].name;
                var boxCollider1 = (BoxCollider)personArray[j].AddComponent<BoxCollider>();
                if (str == "BloodMageJonas/LOD1_unwrapped")
                {
                    boxCollider1.size = new Vector3(0.1f, 0.3f, 0.1f);
                }
                else
                {
                    boxCollider1.size = new Vector3(1, 3, 1);
                }

                var go = new GameObject();
                go.name = "linee";
                var lr = go.AddComponent<LineRenderer>();


                var person_object = GameObject.Find(clusterMap[cluster_list[i]].mPerson[j].name);
                var cluster_object = GameObject.Find(cluster_list[i]);
                lr.SetPosition(0, person_object.transform.position);
                lr.SetPosition(1, cluster_object.transform.position);
                
                lr.SetWidth(0.01f, 0.01f);
                go.transform.SetParent(ground.transform);
                lr.useWorldSpace = false;


            }
        }
    }


}










